# Docker AMP Stack

Docker-based AMP-Stack for working with Apache HTTP Server, MySQL and PHP programming language on linux. In addition this stack includes some usefull tools like Mailhog and phpMyAdmin.

## Includes

* [Apache](https://httpd.apache.org/)
* [MySQL](https://www.mysql.com/)
* [PHP](https://www.php.net/)
* [phpMyAdmin](https://www.phpmyadmin.net/)
* [Mailhog](https://github.com/mailhog/MailHog)

## System Requirements

To use this AMP-Stack, the following system requirements must be met.

* You need to have [Docker](https://www.docker.com/) installed.
* You need to have [Docker-Compose](https://docs.docker.com/compose/gettingstarted/) installed.

## Installation
```bash
## Clone repository
git clone https://gitlab.com/LittleToni/docker-lamp-stack.git

## Change to project folder
cd docker-lamp-stack
```

## Configuration

```bash
## Copy environment file
cp .env.example .env
```

> **Check:** Feel free to change some settings.

## Usage

```bash
## Build containers
docker-compose build

## Run containers as deamon
docker-compose up -d

## Stop & remove containers
docker-compose down
```

### Application

* Runs your application on [http://localhost](http://localhost)
* Runs PhpMyAdmin on [http://localhost:xxxx](http://localhost:xxxx)