# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added

- Container names based on the given project name

### Changed

- Change default mysql port to 3309

## [1.1.0] - 2019-12-07

### Added

- Add Node and NPM support
- Default MySQL version from 5.6.42 to 5.7.28
- Change default application name