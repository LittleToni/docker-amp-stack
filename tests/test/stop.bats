#!/usr/bin/env bats

@test "Stop all Docker Containers" {
    docker-compose down
}