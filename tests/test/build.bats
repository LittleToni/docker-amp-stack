#!/usr/bin/env bats

@test "Build Docker Containers" {
    docker-compose build --no-cache
}