#!/usr/bin/env bats

@test "Start all Docker Containers" {
    docker-compose up -d
}